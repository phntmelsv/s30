// insert data
db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);


// total number of fruits on sale (count operator)
db.fruits.aggregate([
    {
        $match: { 
          onSale: true 
        }
    },
    {
         $count: "fruitsOnSale" 
    }
  ]);


// total number of fruits with stock >= 20 (count operator)
db.fruits.aggregate([
    {
        $match: { 
          stock: { $gte:20 }
        }
    },
    {
         $count: "enoughStock" 
    }
  ]);


// average price of fruits onSale per supplier

db.fruits.aggregate([
    {
        $match: { 
          onSale: true 
        }
    },
    {
        $group: { 
          _id: "$supplier_id", 
          avg_price: { $avg: "$price" } 
        }
    }
  ]);

// highest price of a fruit per supplier
db.fruits.aggregate([
    {
        $group: { 
          _id: "$supplier_id", 
          max_price: { $max: "$price" } 
        }
    }
  ]);


// lowest price of a fruit per supplier
db.fruits.aggregate([
    {
        $group: { 
          _id: "$supplier_id", 
          min_price: { $min: "$price" } 
        }
    }
  ]);
